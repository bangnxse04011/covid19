const express = require('express');
const date = require('date-and-time');
const router = express.Router();
const covid19Service = require('../repo/covid19Repo');
const countryInfoRepo = require('../repo/countryInfoRepo');
const request = require('request');

const schedule = require('node-schedule');

schedule.scheduleJob('*/2 * * * *', updateData);

function updateData() {
    console.log("===========================================================");
    console.log("Start at: " + new Date().toISOString());
    request('https://api.covid19api.com/summary', {
        json: true,
        rejectUnauthorized: false,
        requestCert: false,
        agent: false,
    }, (err, res, body) => {
        if (err) { return console.log(err); }
        if (body) {
            // console.log(body);
            covid19Service.deleteCovid19().then(res => {
                if (res) {};
                console.log("DELETE ALL DATA");
                Object.values(body.Countries).forEach(element => {
                    if (element) {
                        covid19Service.findBySlug(element.Slug).then(result => {
                            if (result) {
                                console.log("FIND DATA BY SLUG: " + element.Slug);
                                let json = JSON.parse(JSON.stringify(result));
                                json.confirmed = json.confirmed + element.TotalConfirmed;
                                json.new_confirmed = json.new_confirmed + element.NewConfirmed;
                                json.deaths = json.deaths + element.TotalDeaths;
                                json.new_deaths = json.new_deaths + element.NewDeaths;
                                json.recovered = json.recovered + element.TotalRecovered;
                                json.new_recovered = json.new_recovered + element.NewRecovered;
                                covid19Service.updateDataCovid19(json).then(_ => {});
                            } else {
                                // Case not exit data in DB
                                console.log("CREATE NEW DATA");
                                const now = new Date();
                                let country = element.Country;
                                if (element.Slug == 'russia') {
                                    country = 'Russia';
                                }
                                if (element.Slug == 'us' || element.Slug == 'united-states') {
                                    country = 'US';
                                }
                                if (element.Slug == 'vietnam') {
                                    country = 'Vietnam';
                                }
                                var today = new Date();
                                var dd = today.getDate();
                                var mm = today.getMonth() + 1;
                                var yyyy = today.getFullYear();
                                let json = {
                                    "countryregion": country,
                                    "slug": element.Slug,
                                    "confirmed": element.TotalConfirmed,
                                    "new_confirmed": element.NewConfirmed,
                                    "deaths": element.TotalDeaths,
                                    "new_deaths": element.NewDeaths,
                                    "recovered": element.TotalRecovered,
                                    "new_recovered": element.NewRecovered,
                                    "str_date": yyyy + "-" + mm + "-" + dd
                                }
                                covid19Service.insertNewDataCovid19Only(json).then(result => {});
                            }
                            console.log("===========================================================");
                            console.log("End at: " + new Date());
                        });
                    }
                });
            });
        }
    });

}

module.exports = router;