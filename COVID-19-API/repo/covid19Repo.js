const db = require('../migrate/db');
const date = require('date-and-time');
const Op = db.Sequelize.Op;

module.exports = {

    /**
     * Method create new data covid
     */
    insertNewDataCovid19: (covid19) => {
        return db.covid19.bulkCreate(covid19);
    },

    /**
     * Method create only new data covid 19
     */
    insertNewDataCovid19Only: (covid19) => {
        return db.covid19.create(covid19);
    },

    /**
     * Method find data by id
     */
    findByCountryRegion: (countryregion) => {
        return db.covid19.findAll({
            where: {
                countryregion: {
                    [Op.iLike]: '%' + countryregion + '%'
                }
            }
        });
    },

    /**
     * Method find data by id
     */
    findBySlug: (slug) => {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        return db.covid19.findOne({
            where: {
                slug: slug,
                str_date: yyyy + "-" + mm + "-" + dd
            }
        });
    },

    /**
     * Method find all data
     */
    findAll: () => {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        return db.covid19.findAll({
            where: {
                str_date: yyyy + "-" + mm + "-" + dd
            }
        });
    },


    /**
     * Method update new data covid 19
     */
    updateDataCovid19: (covid19) => {
        return db.covid19.update(covid19, {
            where: {
                id: covid19.id
            }
        });
    },

    /**
     * Method remove all data covid 19
     */
    deleteCovid19: () => {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        return db.covid19.destroy({
            where: {
                str_date: yyyy + "-" + mm + "-" + dd
            }
        });
    },

    /**
     * Method find store info by pro id
     */
    coundAllData: () => {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        let query = `SELECT c.countryregion AS countryregion,
                            sum(c.confirmed) AS confirmed,
                            c.new_confirmed,
                            sum(c.deaths) AS deaths,
                            c.new_deaths,
                            sum(c.recovered) AS recovered,
                            c.new_recovered,
                            ci.link_info
                    FROM 
                        covid19s c
                        LEFT JOIN country_infos ci
                        ON c.countryregion = ci.countryregion
                    WHERE 
                        1 = 1
                        AND c.str_date = '${yyyy}-${mm}-${dd}'
                    AND c.confirmed > 0
                    GROUP BY 1,ci.link_info,c.new_confirmed,c.new_deaths,c.new_recovered
                    ORDER BY sum(c.confirmed) DESC`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },


    /**
     * Method find store info by pro id
     */
    coundAllDataByContry: (name) => {
        let query = `select sum(confirmed) as confirmed, sum(deaths) as deaths, sum(recovered) as recovered, str_date from covid19s where countryregion = '${name}' group by TO_DATE(c.str_date, 'YYYY-MM-DD')`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },


    /**
     * Method find all 
     */
    countAllConfirm: () => {
        let query = `SELECT 
                        sum(c.confirmed)::INTEGER AS y,
                        c.str_date AS label
                    FROM 
                        covid19s c
                    WHERE 
                        1 = 1
                    AND c.confirmed > 0
                    GROUP BY c.str_date
                    ORDER BY to_date(c.str_date, 'yyyy-mm-dd')`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * Method find all 
     */
    countAllDeaths: () => {
        let query = `SELECT 
                        sum(c.deaths)::INTEGER AS y,
                        c.str_date AS label
                    FROM 
                        covid19s c
                    WHERE 
                        1 = 1
                    AND c.confirmed > 0
                    GROUP BY c.str_date
                    ORDER BY to_date(c.str_date, 'yyyy-mm-dd')`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * Method find all 
     */
    countAllRecovery: () => {
        let query = `SELECT 
                        sum(c.recovered)::INTEGER AS y,
                        c.str_date AS label
                    FROM 
                        covid19s c
                    WHERE 
                        1 = 1
                    AND c.confirmed > 0
                    GROUP BY c.str_date
                    ORDER BY to_date(c.str_date, 'yyyy-mm-dd')`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * 
     */
    countAllConfirmDES() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        let query = `select sum(c.confirmed)::INTEGER AS y, c.countryregion as label from covid19s c where c.str_date = '${yyyy}-${mm}-${dd}' GROUP BY c.countryregion, c.confirmed ORDER BY c.confirmed DESC limit 5`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * 
     */
    countAllDeathsDES() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        let query = `select sum(c.deaths)::INTEGER AS y, c.countryregion as label from covid19s c where c.str_date = '${yyyy}-${mm}-${dd}' GROUP BY c.countryregion, c.confirmed ORDER BY c.confirmed DESC limit 5`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * 
     */
    countAllRecoveredDES() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        let query = `select sum(c.recovered)::INTEGER AS y, c.countryregion as label from covid19s c where c.str_date = '${yyyy}-${mm}-${dd}' GROUP BY c.countryregion, c.confirmed ORDER BY c.confirmed DESC limit 5`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },
    /**
     * 
     */
    findAllCountryRegion() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        let query = `select c.countryregion from covid19s c where c.confirmed > 0 and c.str_date = '${yyyy}-${mm}-${dd}'`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     *  Method find all country region name confirm
     */
    findAllCountryRegionNameConfirm(name, str_date) {
        let query = `select c.confirmed::INTEGER AS y, c.countryregion as label from covid19s c where c.countryregion IN (${name}) and TO_DATE(c.str_date, 'YYYY-MM-dd') = TO_DATE('${str_date}', 'YYYY-MM-dd') ORDER BY y DESC`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * 
     */
    findAllCountryRegionNameDeath(name,str_date) {
        let query = `select c.deaths as y, c.str_date as label from covid19s c where c.countryregion = '${name}' and c.str_date <= '${str_date}'`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * 
     */
    findAllCountryRegionNameRecover(name,str_date) {
        let query = `select c.recovered as y, c.str_date as label from covid19s c where c.countryregion = '${name}' and c.str_date <= '${str_date}'`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * 
     */
    findAllDataDateByName(name) {
        let query = `SELECT c.str_date FROM covid19s c WHERE c.countryregion IN (${name}) GROUP BY c.str_date ORDER BY TO_DATE(c.str_date, 'YYYY-MM-dd') ASC `;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * 
     */
    findAllDate() {
        let query = `SELECT c.str_date FROM covid19s c WHERE 1 = 1 GROUP BY c.str_date ORDER BY TO_DATE(c.str_date, 'YYYY-MM-dd') ASC `;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    },

    /**
     * 
     */
    async findAllEntityByDate(str_date) {
        let query = `select c.countryregion as name, c.slug as code, c.confirmed::INTEGER AS value from covid19s c where 1 = 1 and TO_DATE(c.str_date, 'YYYY-MM-dd') = TO_DATE('${str_date}', 'YYYY-MM-dd')`;
        return db.connection.query(query, {
            type: db.connection.QueryTypes.SELECT
        });
    }
}