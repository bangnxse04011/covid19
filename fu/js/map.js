$(document).ready(function() {
    $.ajax({
        url: "http://localhost:9876/api/v1/covid19/report/",
        dataType: "JSON"
    }).done(function(data) {
        google.load('visualization', '1', { 'packages': ['geochart'], 'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY' });
        google.setOnLoadCallback(drawStatesMap);

        function drawStatesMap() {
            var options = { colors: ['#FF0000'] };
            var confirmed = "confirmed";
            var statesArray = [
                ["Country", "Quantity"]
            ];
            $.each(data, function() {
                var stateitem = [this.countryregion, +this[confirmed]];
                statesArray.push(stateitem);
            });
            var statesData = google.visualization.arrayToDataTable(statesArray);
            var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
            chart.draw(statesData, options);

            $('#example').DataTable({
                "pagingType": "full_numbers"
            });
        }

    });

    $.ajax({
        url: "http://localhost:9876/api/v1/covid19/report/find-all-confirm",
        dataType: "JSON"
    }).done(function(data) {
        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            theme: "light2", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Tổng Số Ca Nhiễm"
            },
            axisY: {
                title: "(Người)"
            },
            data: [{
                type: "column",
                showInLegend: true,
                legendMarkerColor: "grey",
                legendText: "Bảng thống kê số lượng người nhiễm trên thế giới.",
                dataPoints: data
            }]
        });
        chart.render();
    });

    $.ajax({
        url: "http://localhost:9876/api/v1/covid19/report/find-all-deaths",
        dataType: "JSON"
    }).done(function(data) {
        var chart = new CanvasJS.Chart("chartContainer1", {
            animationEnabled: true,
            theme: "light2", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Tổng Số Người Chết"
            },
            axisY: {
                title: "(Người)"
            },
            data: [{
                type: "column",
                showInLegend: true,
                legendMarkerColor: "grey",
                legendText: "Bảng thống kê số lượng người nhiễm trên thế giới.",
                dataPoints: data
            }]
        });
        chart.render();
    });

    $.ajax({
        url: "http://localhost:9876/api/v1/covid19/report/find-all-recovery",
        dataType: "JSON"
    }).done(function(data) {
        var chart = new CanvasJS.Chart("chartContainer2", {
            animationEnabled: true,
            theme: "light2", // "light1", "light2", "dark1", "dark2"
            title: {
                text: "Tổng Số Hồi Phục"
            },
            axisY: {
                title: "(Người)"
            },
            data: [{
                type: "column",
                showInLegend: true,
                legendMarkerColor: "grey",
                legendText: "Bảng thống kê số lượng người nhiễm trên thế giới.",
                dataPoints: data
            }]
        });
        chart.render();
    });

    $("#filterType").change(function() {
        let value = $("#filterType").val();
        if (value == '1') {
            url = "http://localhost:9876/api/v1/covid19/report/bd-confirm";
        } else if (value == '2') {
            url = "http://localhost:9876/api/v1/covid19/report/bd-deaths";
        } else if (value == '3') {
            url = "http://localhost:9876/api/v1/covid19/report/bd-recovered";
        }
        $.ajax({
            url: url,
            dataType: "JSON"
        }).done(function(data) {
            var chart = new CanvasJS.Chart("chartContainer5", {
                title: {
                    text: "Bảng số ca"
                },
                axisX: {
                    interval: 1
                },
                axisY2: {
                    interlacedColor: "rgba(1,77,101,.2)",
                    gridColor: "rgba(1,77,101,.1)",
                    title: "Number of Companies"
                },
                data: [{
                    type: "bar",
                    name: "companies",
                    axisYType: "secondary",
                    color: "#014D65",
                    dataPoints: data
                }]
            });
            chart.render();
        });
    });

});