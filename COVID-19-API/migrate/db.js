const Sequelize = require('sequelize');
const db = {}
    /**
     * Config DB connect to postgres
     */
const connection = new Sequelize("COVID19", "postgres", "bangnx1", {
    host: "localhost",
    dialect: 'postgres',
    dialectOptions: {
        multipleStatements: true
    },
    pool: {
        max: 20,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

//----------------------------------------------------------------------------------------
db.Sequelize = Sequelize;
db.connection = connection;

// Tạo bảng khi run code
//----------------------------------------------------------------------------------------
db.covid19 = require('./table/covid19_table')(connection, Sequelize);
db.country_info = require('./table/country_info')(connection, Sequelize);

//----------------------------------------------------------------------------------------
// Tao quan he

connection.authenticate()
    .then(() => {
        console.log('Connection successfully.');
    })
    .catch(err => {
        console.error('ERROR:', err);
    });

module.exports = db;