const express = require('express');
const router = express.Router();
const covid19Service = require('../repo/covid19Repo');

// schedule.scheduleJob('1 * * * *', updateCSVDataSet);

/**
 * Method find all data
 */
router.get('/', function(req, res) {
    console.log("RUN METHOD FIND ALL DATA");
    covid19Service.coundAllData().then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});


/**
 * Method find all data
 */
router.get('/find-all-confirm', function(req, res) {
    covid19Service.countAllConfirm().then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

/**
 * Method find all data
 */
router.get('/find-all-deaths', function(req, res) {
    covid19Service.countAllDeaths().then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

/**
 * Method find all data
 */
router.get('/find-all-recovery', function(req, res) {
    covid19Service.countAllRecovery().then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

/**
 * Method find all data
 */
router.get('/bd-confirm', function(req, res) {
    covid19Service.countAllConfirmDES().then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

/**
 * Method find all data
 */
router.get('/bd-deaths', function(req, res) {
    covid19Service.countAllDeathsDES().then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

/**
 * Method find all data
 */
router.get('/bd-recovered', function(req, res) {
    covid19Service.countAllRecoveredDES().then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

/**
 * Method find all data
 */
router.get('/find-country', function(req, res) {
    covid19Service.findAllCountryRegion().then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

router.get('/find-country/:country/data/:str_date', function(req, res) {
    let countryregion = req.params['country'];
    let str_date = req.params['str_date'];
    covid19Service.findAllCountryRegionNameConfirm(countryregion, str_date).then(result => {
        if(result) {
            res.status(200).send(result);
        }
    });
});

/**
 * Method find all data
 */
router.get('/find-country/:country/deaths', function(req, res) {
    let countryregion = req.params['country'];
    covid19Service.findAllCountryRegionNameDeath(countryregion).then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

/**
 * Method find all data
 */
router.get('/find-country/:country/rec', function(req, res) {
    let countryregion = req.params['country'];
    covid19Service.findAllCountryRegionNameRecover(countryregion).then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

router.get('/find-country/:country/date', function(req, res) {
    let countryregion = req.params['country'];
    covid19Service.findAllDataDateByName(countryregion).then(result => {
        if (result) {
            res.status(200).send(result)
        }
    });
});

router.get('/bar/chart/race', function(req, res) {
    let data = [];
    covid19Service.findAllDate().then(result => {
        if (result) {
            result.forEach(element => {
                covid19Service.findAllEntityByDate(element.str_date).then(result1 => {
                    console.log("run========");
                    let json = {
                        "year" : element.str_date,
                        "entries" : JSON.parse(JSON.stringify(result1)) 
                    }
                    data.push(json);
                });
            });
            console.log(data);
            res.status(200).send(data);
        }
    });
});


module.exports = router;