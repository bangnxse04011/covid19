$(document).ready(function() {

    $("#show").hide();
    $("#dateShow").hide();
    
    let url1 = "http://localhost:9876/api/v1/covid19/report/bd-confirm";
    let url2 = "http://localhost:9876/api/v1/covid19/report/bd-deaths";
    let url3 = "http://localhost:9876/api/v1/covid19/report/bd-recovered";
    $.ajax({
        url: url1,
        dataType: "JSON"
    }).done(function(data1) {
        $.ajax({
            url: url2,
            dataType: "JSON"
        }).done(function(data2) {
            $.ajax({
                url: url3,
                dataType: "JSON"
            }).done(function(data3) {
                var chart = new CanvasJS.Chart("chartContainer5", {
                    animationEnabled: true,
                    title: {
                        text: "Bảng số liệu"
                    },
                    axisY: {
                        title: "Số người"
                    },
                    legend: {
                        cursor: "pointer"
                    },
                    toolTip: {
                        shared: true
                    },
                    data: [{
                            type: "bar",
                            showInLegend: true,
                            name: "Ca nhiễm",
                            color: "red",
                            dataPoints: data1
                        },
                        {
                            type: "bar",
                            showInLegend: true,
                            name: "Người chết",
                            color: "yellow",
                            dataPoints: data2
                        },
                        {
                            type: "bar",
                            showInLegend: true,
                            name: "Phục Hồi",
                            color: "green",
                            dataPoints: data3
                        }
                    ]
                });
                chart.render();
            });
        });
    });

    $.ajax({
        url: "http://localhost:9876/api/v1/covid19/report/find-country",
        dataType: "JSON"
    }).done(function(data) {
        data.forEach(element => {
            $("#country1").append(new Option(element.countryregion, element.countryregion));
        });
    });

    /**
     * 
     */
    $("#submit").click(function() {
        $("#show").show();
        let country = $("#country1").val();
        let strCountry = "";
        if(country.length > 0) {
            country.forEach(element => {
                strCountry += "'" + element + "'" + ",";
            });
        }
        strCountry = strCountry.slice(0,-1);
        $.ajax({
            url: "http://localhost:9876/api/v1/covid19/report/find-country/" + strCountry + "/date",
            dataType: "JSON"
        }).done(function(data) {
            let i = 1;
            updateData(strCountry, data[0].str_date);
            setInterval(function(){
                console.log("Sleep");
                if(i < data.length) {
                    date = data[i].str_date;
                    console.log("Search date:" + date);
                    updateData(strCountry, date);
                    i++;
                }
            }, 3000);
        });

        function updateData(strCountry, str_date) {
            $.ajax({
                url: "http://localhost:9876/api/v1/covid19/report/find-country/" + strCountry + "/data/" + str_date,
                dataType: "JSON"
            }).done(function(data) {
                draw(data, str_date);
            });
        }

        function draw(dataDraw, str_date) {
            $("#dateSearch").html(str_date);
            $("#dateShow").show();
            var chart = new CanvasJS.Chart("chartContainer11", {
                animationEnabled: false,
                animationDuration: true,
                dataPointMaxWidth: 800,
                title: {
                    text: "Bảng số liệu"
                },
                axisY: {
                    title: "Số người",
                    maximum: 2000000,
                    // interval: 100,
                },
                legend: {
                    cursor: "pointer"
                },
                toolTip: {
                    shared: true
                },
                data: [{
                        type: "bar",
                        showInLegend: false,
                        name: "Ca nhiễm",
                        color: "#ff3300",
                        dataPoints: dataDraw
                    }
                ]
            });
            chart.render();
        }

    });
});