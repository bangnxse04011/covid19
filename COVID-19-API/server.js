const express = require('express');
const db = require('./migrate/db');
const cors = require('cors');

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const port = 9876;

const covid19Report = require('./api/covid19Report');
const apiCovid = require('./api/apiCovid');

app.use('/api/v1/covid19/report', covid19Report);
app.use('/api/v1/covid19/', apiCovid);

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', (req, res) => res.send('STARTED SERVER'));

app.listen(port, () => console.log(`API Server listening on port ${port}!`));