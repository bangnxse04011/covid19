const express = require('express');
const date = require('date-and-time');
const router = express.Router();
const covid19Service = require('../repo/covid19Repo');
const countryInfoRepo = require('../repo/countryInfoRepo');

const csv = require('csv-parser');
const fs = require('fs');

let rawdata = fs.readFileSync('../dataset/timeseries.json');
let data = JSON.parse(rawdata);

Object.keys(data).forEach(function(key) {
    console.log("==============");
    let value = data[key];
    Object.values(value).forEach(element => {
        let json = {
            "countryregion": key,
            "slug": key.toLowerCase(),
            "confirmed": element.confirmed,
            "deaths": element.deaths,
            "recovered": element.recovered,
            "str_date": element.date
        }
        covid19Service.insertNewDataCovid19Only(json).then(result => {});
    });
    console.log("==============");
});