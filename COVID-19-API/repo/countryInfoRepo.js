const db = require('../migrate/db');

module.exports = {

    /**
     * Method create only new data covid 19
     */
    insertNewDataCountryInfo: (countryInfo) => {
        return db.country_info.create(countryInfo);
    },

}