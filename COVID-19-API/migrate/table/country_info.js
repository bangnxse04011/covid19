module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table CountryInfo
     */
    const CountryInfo = connection_db.define('country_info', {

        countryregion: {
            type: Sequelize.STRING
        },
        covid19_id: {
            type: Sequelize.INTEGER
        },
        link_info: {
            type: Sequelize.STRING
        }
    });

    CountryInfo.sync();

    return CountryInfo;
};