module.exports = (connection_db, Sequelize) => {
    /**
     * Create Table Covid19
     */
    const Covid19 = connection_db.define('covid19', {

        countryregion: {
            type: Sequelize.STRING
        },
        slug: {
            type: Sequelize.STRING
        },
        confirmed: {
            type: Sequelize.INTEGER
        },
        new_confirmed: {
            type: Sequelize.INTEGER
        },
        deaths: {
            type: Sequelize.INTEGER
        },
        new_deaths: {
            type: Sequelize.INTEGER
        },
        recovered: {
            type: Sequelize.INTEGER
        },
        new_recovered: {
            type: Sequelize.INTEGER
        },
        str_date: {
            type: Sequelize.STRING
        }
    });

    Covid19.sync();

    return Covid19;
};